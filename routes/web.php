<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::get('/','HomeController@index');

//Route::get('/', 'DashboardController@index');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth'])->group(function () {

Route::get('/master', function () {
    return view('layout.master');
});

//CRUD Film
Route::resource('postingan', 'PostinganController');

//CRUD update profile
Route::resource('profile', 'ProfileController')->only([
    'index', 'update'
]);

Route::resource('komentar', 'KomentarController')->only([
    'store'
]);
});