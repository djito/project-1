<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likekomentar extends Model
{
    //
    protected $table = 'likekomentar';
    protected $fillable = ['user_id', 'komentar_id'];
}
