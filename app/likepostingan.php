<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likepostingan extends Model
{
    //
    protected $table = 'likepostingan';
    protected $fillable = ['user_id', 'postingan_id'];
}
