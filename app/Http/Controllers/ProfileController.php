<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Profile;
use Auth;
use File;

class ProfileController extends Controller
{
    //
    public function index(){
        $profile = Profile::where('user_id', Auth::id())->first();

        return view('profile.index', compact('profile'));
    }

    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
            'nama' =>'required',
            'pp' => '|image|mimes:jpeg,png,jpg'
            
        ]);


        if($request->has('pp')){

            $profile = Profile::find($id);

            $path = "gambar/";
            File::delete($path . $profile->pp);

            $NamaPp = time().'.'.$request->pp->extension();  
   
            $request->pp->move(public_path('gambar'), $NamaPp);

            $profile->nama = $request->nama;
            $profile->umur = $request->umur;
            $profile->bio = $request->bio;
            $profile->alamat = $request->alamat;
            $profile->pp = $NamaPp;
            
            $profile->save();

            Alert::success('Berhasil!', 'Update profile lucu Berhasil');

            return redirect('/profile');

        }else{
            $profile = Profile::find($id); 

            $profile->nama = $request->nama;
            $profile->umur = $request->umur;
            $profile->bio = $request->bio;
            $profile->alamat = $request->alamat;        
            
            $profile->save();   
            
            Alert::success('Berhasil!', 'Update profile lucu Berhasil');

            return redirect('/profile');
        }

    }
}
