<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Postingan;
use File;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;
use App\User;
use App\Profile;

class PostinganController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $postingan = Postingan::all();

        return view('postingan.index', compact('postingan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('postingan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'tulisan' => 'required',            
            'caption' => 'required',
            'quote'=> 'required',
            
            'gambar' => 'required|image|mimes:jpeg,png,jpg'
        ]);

        //$user_id = {{Auth::user()->id}};
        $NamaGambar = time().'.'.$request->gambar->extension();  
   
        $request->gambar->move(public_path('gambar'), $NamaGambar);

        $postingan = new Postingan;

        $postingan->tulisan = $request->tulisan;
        $postingan->caption = $request->caption;
        $postingan->quote = $request->quote; 
        $postingan->user_id = Auth::user()->id;
        $postingan->gambar = $NamaGambar;

        $postingan->save();

        Alert::success('Berhasil!', 'Tambah postingan lucu Berhasil');

        return redirect('/postingan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $postingan= Postingan::findOrFail($id);
        return view('postingan.show', compact('postingan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $postingan= Postingan::findOrFail($id);
        return view('postingan.edit', compact('postingan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'tulisan' => 'required',            
            'caption' => 'required',
            'quote'=> 'required',
            'gambar' => '|image|mimes:jpeg,png,jpg'
        ]);
        
        if($request->has('gambar')){

            $postingan = Postingan::find($id);

            $path = "gambar/";
            File::delete($path . $postingan->gambar);

            $NamaGambar = time().'.'.$request->gambar->extension();  
   
            $request->gambar->move(public_path('gambar'), $NamaGambar);

            $postingan->tulisan = $request->tulisan;
            $postingan->caption = $request->caption;
            $postingan->quote = $request->quote;
            $postingan->gambar = $NamaGambar;
            
            $postingan->save();

            return redirect('/postingan');
        }else{
            $postingan = Postingan::find($id);
            $postingan->tulisan = $request->tulisan;
            $postingan->caption = $request->caption;
            $postingan->quote = $request->quote;

            $postingan->save();    
            
            Alert::success('Berhasil!', 'Update postingan lucu Berhasil');

            return redirect('/postingan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $postingan = Postingan::find($id);

        $path = "gambar/";
        File::delete($path . $postingan->gambar);

        $postingan->delete();

        Alert::success('Berhasil!', 'Hapus postingan lucu Berhasil');

        return redirect('/postingan');
    }
}
