<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postingan extends Model
{
    //
    protected $table = 'postingan';
    protected $fillable = ['tulisan', 'gambar', 'caption', 'quote', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }

}
