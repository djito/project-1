@extends('layout.master')

@section('inputdata')

<div class="card mx-4 mt-3 bg-light" style="width: 75%">  
    <h3>Update Profile</h3>
    <form action="/profile/{{$profile->id}}" method="post" enctype="multipart/form-data">
        @csrf   
        @method('PUT')    

        <!--Update data User -->
        <div class="input-group mb-3" >
            
            <input type="text" class="form-control"  value="{{$profile->user->name}}" disabled>         
        </div>

        <div class="input-group mb-3">
            
            <input type="text"  class="form-control"  value="{{$profile->user->email}}" disabled>            
        </div>

        <!--Update data profile -->
        <div class="input-group mb-3">
           
            <input type="text"  class="form-control" name="nama"  value="{{$profile->nama}}" >          
            
            </div>
    
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="input-group mb-3">
            
            <input type="number"  class="form-control" name="umur"  value="{{$profile->umur}}" >          
        
        </div>

        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="input-group mb-3">
            
            <textarea name="bio" class="form-control" cols="30" rows="10">{{$profile->bio}}</textarea>          
        
        </div>

        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="input-group mb-3">
            
            <textarea name="alamat" class="form-control" cols="30" rows="10" >{{$profile->alamat}}</textarea>          
        
        </div>

        @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">                
            <input type="file" name="pp" class="form-control">
            <small class="form-text text-muted">Profile Pic terlucu</small>
        </div>
        @error('pp')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        
        
        <!-- /.col -->
        <button type="submit"  class="btn btn-primary">Update</button>
        
        <!-- /.col -->
       
    </form>              
        
</div>
   
@endsection