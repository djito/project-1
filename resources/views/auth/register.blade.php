<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Register - LucuBanget</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{asset('admin/vendors/feather/feather.css')}}">
  <link rel="stylesheet" href="{{asset('admin/vendors/ti-icons/css/themify-icons.css')}}">
  <link rel="stylesheet" href="{{asset('admin/vendors/css/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{asset('admin/css/vertical-layout-light/style.css')}}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('admin/images/favicon.png')}}" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div>
                <img src="{{asset('img/logo-lucubanget.png')}}" width="280" alt="logo">
              </div><br>
              <h4>Baru Yak?</h4>
              <h6 class="font-weight-light">Daftarlah. Cuman sebentar saja</h6>

              <form action="{{route('register')}}" method="post">
                @csrf
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg" name="name" placeholder="Username">
                    </div>
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group">
                        <input type="email" class="form-control form-control-lg" name="email" placeholder="Email">
                    </div>
                    @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="password" placeholder="Password (8 karakter)">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                          </div>
                        </div>
                      </div>
                    @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="input-group mb-3">
                        <input type="password" class="form-control" class="form-control" name="password_confirmation" placeholder="Retype password (8 karakter)">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                          </div>
                        </div>
                      </div>
                      

                    <!--Input data profile -->
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="nama" placeholder="Nama">          
                        
                    </div>
            
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="input-group mb-3">
                        <input type="number" class="form-control" name="umur" placeholder="Umur">          
                        
                    </div>
            
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
            
                    <div class="input-group mb-3">
                        <textarea name="bio" class="form-control" placeholder="Bio"></textarea>          
                        
                    </div>
            
                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
            
                    <div class="input-group mb-3">
                        <textarea name="alamat" class="form-control" placeholder="Alamat"></textarea>          
                        
                    </div>
            
                    @error('alamat')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    
                    <div class="row">
                        <!-- /.col -->
                        <button type="submit"  class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">Register</button>
                        
                        <!-- /.col -->
                      </div>

                    <div class="text-center mt-4 font-weight-light">
                    Sudah punya akun disini? <a href="/login" class="text-primary">Login</a>
                    </div>

               </form>

            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{asset('admin/js/off-canvas.js')}}"></script>
  <script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('admin/js/template.js')}}"></script>
  <script src="{{asset('admin/js/settings.js')}}"></script>
  <script src="{{asset('admin/js/todolist.js')}}"></script>
  <!-- endinject -->
</body>

</html>
