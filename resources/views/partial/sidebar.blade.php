<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="/postingan">
          <i class="icon-grid menu-icon"></i>
          <span class="menu-title">postingan</span>
        </a>
      </li>

      @auth
        <li class="nav-item">
          <a class="nav-link" href="/profile">
            <i class="icon-grid menu-icon"></i>
            <span class="menu-title">Profile</span>
          </a>
        </li> 
      @endauth
      

      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
          <i class="icon-layout menu-icon"></i>
          <span class="menu-title">Kategori</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="ui-basic">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="#">Foto</a></li>
            <li class="nav-item"> <a class="nav-link" href="#">Meme</a></li>
            <li class="nav-item"> <a class="nav-link" href="#">Quote</a></li>
            <li class="nav-item"> <a class="nav-link" href="#">Karikatur</a></li>
            <li class="nav-item"> <a class="nav-link" href="#">Serba-serbi</a></li>
          </ul>
        </div>
      </li>
      
      

      @auth
      <li class="nav-item">
        <a class="nav-link bg-danger text-white" href="{{ route('logout') }}"
        onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
          <i class="icon-head menu-icon text-white"></i>
          <span class="menu-title">Logout</span>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
      </li> 

      @endauth
     
      @guest
              <!-- /.sidebar-menu LGIN-->
          <li class="nav-item">
            <a class="nav-link bg-warning text-white" href="/login">
              <i class="icon-head menu-icon text-white"></i>
              <span class="menu-title">Login</span>
            </a>
          </li>
        @endguest




    </ul>
  </nav>