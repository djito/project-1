<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
      <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021. LucuBanget.com Koleksi Meme Foto dan Video Lucu. All rights reserved.</span>
      <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Made with <i class="ti-heart text-danger ml-1"></i> by Kelompok 3</span>
    </div>
  </footer> 