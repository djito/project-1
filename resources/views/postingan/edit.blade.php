@extends('layout.master')

@section('inputdata')
@push('script')
    <script src="https://cdn.tiny.cloud/1/9b4nar3yammgjkqx1dsjgg2ki5wk4h01daagpc28amy1kg6g/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

<div class="card mx-4 mt-3 bg-light"> 
        <form action="/postingan/{{$postingan->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT') 

            <div class="form-group">                
                <input type="text" name="quote" class="form-control" style="width: 75%" value="{{$postingan->quote}}" >
            </div>
            @error('quote')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror             
                

            <div class="form-group">                
                <input type="file" name="gambar" class="form-control" style="width: 75%">
                <small id="emailHelp" class="form-text text-muted">Postingan gambar terlucu</small>
            </div>
            @error('gambar')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">                
                <input type="text" name="caption" class="form-control" style="width: 75%" value="{{$postingan->caption}}" >
            </div>
            @error('caption')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">            
                <textarea name="tulisan" class="form-control" cols="20" rows="3" style="width: 75%">{{$postingan->tulisan}}</textarea>
            </div>
            @error('tulisan')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

                             

                
            <button type="submit" class="btn btn-primary">Update</button>
        </form>

    </div>  
    @stack('script')
 @endsection