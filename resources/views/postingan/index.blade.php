@extends('layout.master')

@section('inputdata')
    @auth
        <a class="btn btn-success mx-4 mt-3" href="/postingan/create">Tambah Postingan Lucu</a>
    @endauth
@endsection

@section('content')

    @forelse ($postingan as $item)
          
    <div class="card mx-2 mb-4" style="width: 400px;">
        <a href="/postingan/{{$item->id}}">
        <img src="{{asset('gambar/'.$item->gambar)}}" class="card-img-top" alt="Detail postingan terlucu"></a>
        <div class="card-body">
            <a href="/postingan/{{$item->id}}" style="text-decoration: none">
            <h5 class="card-title">{{Str::limit($item->quote, 45)}}</h5></a>
            <span class="card-text"><small class="text-muted">by: {{$item->user->name}}</small> . </span>
            <span class="card-text"><small class="text-muted">{{$item->updated_at}}</small> . </span>            
            <span class="badge badge-info">LIKE</span>
            <span class="badge badge-info" style="width: 100px">KOMENTAR {{$item->komentar->count('postingan_id')}}</span>
            

            @auth
            <form action="/postingan/{{$item->id}}" method="post">
                @method('delete')
                @csrf                
                <a href="/postingan/{{$item->id}}/edit" class="btn btn-success btn-sm">edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">                
            </form> 
            @endauth
            
        </div>
    </div>
   
    @empty
        <h3>Belum ada postingan lucu </h3><br> <br>
        <p><a class="btn btn-info btn-sm" href="/postingan/create">Tambah Postingan Lucu</a></p>   
    @endforelse

       
 @endsection