@extends('layout.master')


@section('inputdata')



<div class="card-body mx-4 mt-3 bg-light">
    <figure class="figure">
        <img src="{{asset('gambar/'.$postingan->gambar)}}" class="figure-img img-fluid rounded">
        <figcaption class="figure-caption">{{$postingan->caption}}</figcaption>
    </figure>
    <br>
    <span class="card-text"><small class="text-muted">by: {{$postingan->user->name}}</small> . </span>
    <span class="card-text"><small class="text-muted">{{$postingan->updated_at}}</small> . </span>
    <span class="badge badge-info" style="width: 100px">LIKE</span>  
    <span class="badge badge-info" style="width: 100px">KOMENTAR {{$postingan->komentar->count('postingan_id')}}</span>   
    <h4>{{$postingan->quote}}</h4>
    <p>{!!$postingan->tulisan!!}</p><br><br>
    

    <!-- KOMENTAR -->
    <h2>Komentar</h2>
    @forelse ($postingan->komentar as $item)
    <div class="card-body mt-0">        
        <small><b>{{$item->user->name}}</b></small> <br>    
        {!!$item->isi!!}
    </div>
    @empty
        <h4>Tidak ada komentar</h4>
    @endforelse


    @auth      
    
        <form action="/komentar" method="POST" class="my-3" >
            @csrf
            
            <div class="form-group">
                <label>Komentar</label>
                <input type="hidden" value="{{$postingan->id}}" name="postingan_id">
                <textarea name="isi" class="form-control" cols="30" rows="10"></textarea>
            </div>
            @error('isi')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror  

            <script>
                tinymce.init({
                selector: 'textarea',
                plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
                toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
                toolbar_mode: 'floating',
                tinycomments_mode: 'embedded',
                tinycomments_author: 'Author name',
                });
            </script>
                                
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <!-- KOMENTAR .END -->

    @endauth

    <a class="btn btn-info btn-sm" href="/postingan" style="width: 100px">Kembali</a>
</div>
       
 @endsection