<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>LucuBanget.com</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{asset('admin/vendors/feather/feather.css')}}">
  <link rel="stylesheet" href="{{asset('admin/vendors/ti-icons/css/themify-icons.css')}}">
  <link rel="stylesheet" href="{{asset('admin/vendors/css/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="{{asset('admin/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('admin/vendors/ti-icons/css/themify-icons.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('admin/js/select.dataTables.min.css')}}">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{asset('admin/css/vertical-layout-light/style.css')}}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('admin/images/favicon.png')}}" />
</head>
<body>
  <div class="container-scroller">


    <!-- partial:partials/_navbar.html -->
    @include('partial.nav')
    <!-- partial -->

    <div class="container-fluid page-body-wrapper">

        <!-- partial:partials/_settings-panel.html -->

        @include('partial.setting')
        
        <!-- partial:partials/_sidebar.html -->
        @include('partial.sidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-md-12 grid-margin">
                        <div class="row">

                            <div class="card mx-2 mb-4" style="width: 400px;">
                                <img class="card-img-top" src="img/lucu01.jpg" alt="Card image cap">
                                <div class="card-body">
                                  <h5 class="card-title">Dikira Sakit.. ternyata Cemburu</h5>
                                  <span class="card-text"><small class="text-muted">By: Angga</small> . </span>
                                  <span class="card-text"><small class="text-muted">10 Nov 2021</small> . </span>
                                  <span class="card-text"><small class="text-muted">Share</small> . </span>
                                </div>
                            </div>

                            <div class="card mx-2 mb-4" style="width: 400px;">
                                <img class="card-img-top" src="img/lucu02.jpg" alt="Card image cap">
                                <div class="card-body">
                                  <h5 class="card-title">Buruan Makan Entar Kena Sweeping :)</h5>
                                  <span class="card-text"><small class="text-muted">By: Toms</small> . </span>
                                  <span class="card-text"><small class="text-muted">10 Nov 2021</small> . </span>
                                  <span class="card-text"><small class="text-muted">Share</small> . </span>
                                </div>
                            </div>

                            <div class="card mx-2 mb-4" style="width: 400px;">
                                <img class="card-img-top" src="img/lucu03.jpg" alt="Card image cap">
                                <div class="card-body">
                                  <h5 class="card-title">Ini Namanya Mudik Sekampung wkwkwk</h5>
                                  <span class="card-text"><small class="text-muted">By: Fadhil</small> . </span>
                                  <span class="card-text"><small class="text-muted">10 Nov 2021</small> . </span>
                                  <span class="card-text"><small class="text-muted">Share</small> . </span>
                                </div>
                            </div>

                            <div class="card mx-2 mb-4" style="width: 400px;">
                                <img class="card-img-top" src="img/lucu04.jpg" alt="Card image cap">
                                <div class="card-body">
                                  <h5 class="card-title">Waiini.. Bawa Motor Pake Motor >_<</h5>
                                  <span class="card-text"><small class="text-muted">By: Fadhil</small> . </span>
                                  <span class="card-text"><small class="text-muted">10 Nov 2021</small> . </span>
                                  <span class="card-text"><small class="text-muted">Share</small> . </span>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>

            <!-- content-wrapper ends -->


            <!-- partial:partials/_footer.html -->

            @include('partial.footer')
        <!-- partial -->
        </div>
      <!-- main-panel ends -->
    </div>   
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="{{asset('admin/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="{{asset('admin/vendors/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('admin/vendors/datatables.net/jquery.dataTables.js')}}"></script>
  <script src="{{asset('admin/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
  <script src="{{asset('admin/js/dataTables.select.min.js')}}"></script>

  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{asset('admin/js/off-canvas.js')}}"></script>
  <script src="{{asset('admin/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('admin/js/template.js')}}"></script>
  <script src="{{asset('admin/js/settings.js')}}"></script>
  <script src="{{asset('admin/js/todolist.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('admin/js/dashboard.js')}}"></script>
  <script src="{{asset('admin/js/Chart.roundedBarCharts.js')}}"></script>
  <!-- End custom js for this page-->
</body>

</html>

