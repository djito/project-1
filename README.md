<p align="center"><a href="/" target="_blank"><img src="https://gitlab.com/djito/project-1/-/raw/main/public/img/logo-lucubanget.png" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Final Project

## Kelompok 3

- Abdul Muiz A.M (@Nanashi55) 
- Wahid Ari Anggara P. (@ariepoernama) 
- Sujito (@sdjito)

## Tema Project

LucuBanget merupakan sebuah situs komedi daring yang menampilkan berbagai media visual baik gambar ataupun video

## ERD

<img src="https://gitlab.com/djito/project-1/-/raw/main/public/img/erd-rev.jpg">

## Link Video

Link Demo Aplikasi: <a href="https://youtu.be/tbtJijx2VCw" target="blank"> https://youtu.be/tbtJijx2VCw</a>

Link Display: <a href="https://www.namagraph.com/lucubanget/" target="blank">namagraph.com/lucubanget</a>
